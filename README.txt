This lightweight modules provides a block
that displays links to social profiles (Facebook, Twitter...).

It's intended to be more for developers than general users,
as it doesn't provide all the configuration setups
you can find in other similar modules.

There's no CSS either (and no icon sets),
as it's left for the user to apply its own custom design.

MODULE CONFIGURATION
====================
1. After installing as usual
2. Go to /admin/structure/socialprofiles/settings
3. Set your social profiles links
2. Activate the block

EXTEND
======
* You can easily add or remove manually social services in code
* All the CSS is upon the user
