<?php

namespace Drupal\socialprofiles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SocialProfilesSettingsFrom Class Doc Comment.
 *
 * @category SocialProfilesSettingsFrom
 * @package Form
 */

class SocialProfilesSettingsFrom extends ConfigFormBase {

  /**
   * Define here additional platforms.
   *
   * @return array
   *           the whole set of social platforms
   */
  public function getPlatforms() {
    return array(
      'Twitter',
      'Facebook',
      'Linkedin',
      'Instagram',
      'Youtube',
      'Vimeo',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'socialprofiles_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'socialprofiles.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('socialprofiles.settings');

    $platforms = $this->getPlatforms();

    foreach ($platforms as $platform) {
      $machine_name = strtolower(str_replace(' ', '_', $platform));

      $form['socialprofiles_' . $machine_name] = array(
        '#type' => 'textfield',
        '#title' => $platform,
        '#default_value' => $config->get('socialprofiles.' . $machine_name),
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('socialprofiles.settings');

    $platforms = $this->getPlatforms();

    foreach ($platforms as $platform) {
      $machine_name = strtolower(str_replace(' ', '_', $platform));

      $config->set('socialprofiles.' . $machine_name, $form_state->getValue('socialprofiles_' . $machine_name))->save();
    }

    parent::submitForm($form, $form_state);
  }

}
