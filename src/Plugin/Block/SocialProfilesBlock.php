<?php

namespace Drupal\socialprofiles\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides the social profiles block.
 *
 * @Block(
 *   id = "socialprofiles_block",
 *   admin_label = @Translation("Social profiles block"),
 * )
 */

class SocialProfilesBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */

  public function build() {

    $config = \Drupal::config('socialprofiles.settings');

    $social_links = $config->get('socialprofiles');

    $output = array(
      '#theme' => 'socialprofiles_block',
      '#social_links' => $social_links,
    );

    return array($output);
  }
}
